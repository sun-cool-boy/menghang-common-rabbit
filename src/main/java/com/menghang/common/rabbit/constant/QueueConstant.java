package com.menghang.common.rabbit.constant;

/**
 * @author DongZl
 * @description: 队列常量
 * @Date 2022-10-21 下午 01:59
 */
public class QueueConstant {

    /**
     * 测试队列名称
     */
    public static final String TEST_NAME = "test";

    /**
     * 商品新增队列
     */
    public static final String PRODUCT_ADD = "product_add";
}

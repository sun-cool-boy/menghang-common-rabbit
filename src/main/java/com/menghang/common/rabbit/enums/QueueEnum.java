package com.menghang.common.rabbit.enums;

import com.menghang.common.rabbit.constant.QueueConstant;

/**
 * @author DongZl
 * @description: 队列枚举
 * @Date 2022-10-21 下午 01:44
 */
public enum QueueEnum {


    TEST(QueueConstant.TEST_NAME, "测试队列"),
    PRODUCT_ADD(QueueConstant.PRODUCT_ADD, "商品新增队列"),
    ;

    private String queueName;

    private String queueDesc;

    public String queueName () {
        return queueName;
    }

    public String queueDesc () {
        return queueDesc;
    }

    public String queueStr(){
        return new StringBuilder(this.queueName).append("(").append(this.queueDesc).append(")").toString();
    }

    QueueEnum (String queueName, String queueDesc) {
        this.queueName = queueName;
        this.queueDesc = queueDesc;
    }
}

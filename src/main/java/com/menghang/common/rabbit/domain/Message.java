package com.menghang.common.rabbit.domain;


import com.alibaba.fastjson2.JSONObject;
import com.menghang.common.core.utils.uuid.IdUtils;
import com.menghang.common.core.web.domain.BaseEntity;

import javax.swing.text.html.parser.Entity;
import java.io.Serializable;

/**
 * @author DongZl
 * @description: RabbitMq消息体
 * @Date 2022-10-21 上午 09:14
 */
public class Message<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息ID
     */
    private String id;

    /**
     * 消息创建时间
     */
    private long createTime;

    /**
     * 消息主体
     */
    private T body;

    public Message () {
    }

    public Message (String id, T body) {
        this.id = id;
        this.createTime = System.currentTimeMillis();
        this.body = body;
    }

    /**
     * 自动生成消息ID
     * @param body 消息主体
     * @return
     */
    public static <T> Message<T> builderMsg(T body){
        return new Message(IdUtils.fastUUID(), body);
    }

    /**
     * 生成消息主题
     * @param id 唯一标识
     * @param body 消息主体
     * @return
     */
    public static <T> Message<T> builderMsg(String id, T body){
        return new Message(id, body);
    }


    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public long getCreateTime () {
        return createTime;
    }

    public void setCreateTime (long createTime) {
        this.createTime = createTime;
    }

    public T getBody () {
        return body;
    }

    public void setBody (T body) {
        this.body = body;
    }

    @Override
    public String toString () {
        return "Message{" +
                "id='" + id + '\'' +
                ", createTime=" + createTime +
                ", body=" + JSONObject.toJSONString(body) +
                '}';
    }
}
